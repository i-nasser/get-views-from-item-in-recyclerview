package com.example.getviewsfromiteminrecyclerview.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.getviewsfromiteminrecyclerview.Adapters.ListAdapter;
import com.example.getviewsfromiteminrecyclerview.Interfaces.ViewClickedListener;
import com.example.getviewsfromiteminrecyclerview.R;

import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ListAdapter listAdapter;
    private ArrayList<String> list;
    private int positionClickRecyclerView = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);

        // initialization
        init();
    }

    private void init() {
        list = new ArrayList<>();
        list.add("One");
        list.add("Two");
        list.add("Three");
        list.add("Four");
        list.add("Five");
        list.add("Six");
        list.add("Seven");

        listAdapter = new ListAdapter(getApplicationContext(), list, new ViewClickedListener() {
            @Override
            public void getViewItemClicked(int position) {
                positionClickRecyclerView = position;

                // Get All Views By Position From RecyclerView
                getViewsItemClicked();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(listAdapter);
    }

    // This Method Getting Any Views From RecyclerView By Position
    private void getViewsItemClicked() {
        if (positionClickRecyclerView != -1) {
            RecyclerView.ViewHolder holder = ((ListAdapter) Objects.requireNonNull(recyclerView.getAdapter())).getViewByPosition(positionClickRecyclerView);
            View view = holder.itemView;
            TextView textView = view.findViewById(R.id.TVAppName);

            Toast.makeText(getApplicationContext(), getString(R.string.getting_all_views_success) + " (" + textView.getText().toString() +")", Toast.LENGTH_SHORT).show();
        }
    }
}
