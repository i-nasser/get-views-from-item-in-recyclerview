package com.example.getviewsfromiteminrecyclerview.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.getviewsfromiteminrecyclerview.R;
import com.example.getviewsfromiteminrecyclerview.Interfaces.ViewClickedListener;

import java.util.ArrayList;
import java.util.HashMap;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListClass> {

    private Context context;
    private ArrayList<String> list;
    private ViewClickedListener viewClickedListener;
    private HashMap<Integer, RecyclerView.ViewHolder> holderList;

    @SuppressLint("UseSparseArrays")
    public ListAdapter(Context context, ArrayList<String> list, ViewClickedListener viewClickedListener) {
        this.context = context;
        this.list = list;
        this.viewClickedListener = viewClickedListener;
        holderList = new HashMap<>();
    }

    @NonNull
    @Override
    public ListClass onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_list,parent,false);
        return new ListClass(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListClass holder, final int position) {

        String model = list.get(position);
        holder.textView.setText(model);

        if(!holderList.containsKey(position)){
            holderList.put(position,holder);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, context.getString(R.string.clicked), Toast.LENGTH_SHORT).show();
                viewClickedListener.getViewItemClicked(position); // Send Position View In Activity
            }
        });
    }

    public RecyclerView.ViewHolder getViewByPosition(int position) {
        return holderList.get(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ListClass extends RecyclerView.ViewHolder {
        TextView textView;
        ListClass(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.TVAppName);
        }
    }
}
