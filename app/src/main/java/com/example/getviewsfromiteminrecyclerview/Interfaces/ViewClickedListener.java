package com.example.getviewsfromiteminrecyclerview.Interfaces;

public interface ViewClickedListener {
    void getViewItemClicked(int position);
}
